#!/bin/bash
### base images ###

## python
docker run -it python:latest bash
### ansible-lint
pip install "ansible-lint[community,yamllint]"
git clone https://github.com/debops/ansible-gitlab.git
### checkov
pip install checkov
git clone https://github.com/awslabs/aws-cloudformation-templates.git
### cfn-python-lint
pip install cfn-lint
git clone https://github.com/awslabs/aws-cloudformation-templates.git

## golang
docker run -it golang:latest bash
### kube-linter
GO111MODULE=on go get golang.stackrox.io/kube-linter/cmd/kube-linter
### tfsec
go get -u github.com/tfsec/tfsec/cmd/tfsec
git clone https://github.com/tfsec/tfsec-example-project

## ruby
docker run -it ruby:latest bash
### cfn-scan
gem install cfn-nag
apt update && apt install -y jq
### puppet-lint
gem install puppet-lint
git clone https://github.com/jordansissel/puppet-examples

## ubuntu
docker run -it ubuntu:latest bash
### tflint
apt update && apt install -y curl sudo unzip git
curl https://raw.githubusercontent.com/terraform-linters/tflint/master/install_linux.sh | bash
git clone https://github.com/tfsec/tfsec-example-project
### yamllint
apt update
apt install -y yamllint

## alpine
docker run -it alpine:latest sh
### terrascan
apk add curl git tar
curl --location https://github.com/accurics/terrascan/releases/download/v1.4.0/terrascan_1.4.0_Linux_x86_64.tar.gz --output terrascan.tar.gz
tar -xvf terrascan.tar.gz
install terrascan /usr/local/bin
git clone https://github.com/tfsec/tfsec-example-project