# SAST IaC PoC

Scanning Infrastructure as Code in GitLab CI.

Created as Proof of Concept for https://gitlab.com/gitlab-org/gitlab/-/issues/39695.

## Scanners Evaluated

| Name                                                                     | License    | Language | JSON | JUnit | Examples                                                                                                                                                                |
| ------------------------------------------------------------------------ | ---------- | -------- | ---- | ----- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [ansible-lint](https://github.com/ansible-community/ansible-lint)        | MIT        | Python   | N    |       | [Job Template](https://gitlab.com/greg/sastiac/-/raw/master/ansible-lint.yml) + [Logs](https://gitlab.com/greg-tests/ansible-role-gitlab/-/jobs/1151572054)                                                 |
| [cfn_nag](https://github.com/stelligent/cfn_nag)                         | MIT        | Ruby     | Y    |       | [Job Template](https://gitlab.com/greg/sastiac/-/raw/master/cfn-scan.yml) + [Logs](https://gitlab.com/greg-tests/aws-cloudformation-templates/-/jobs/1151565818)        |
| [cfn-python-lint](https://github.com/aws-cloudformation/cfn-python-lint) | MIT        | Python   | Y    |       | [Job Template](https://gitlab.com/greg/sastiac/-/raw/master/cfn-python-lint.yml) + [Logs](https://gitlab.com/greg-tests/aws-cloudformation-templates/-/jobs/1151565817) |
| [checkov](https://github.com/bridgecrewio/checkov)                       | Apache 2.0 | Python   | Y    | Y     | [Job Template](https://gitlab.com/greg/sastiac/-/raw/master/checkov.yml) + [Logs](https://gitlab.com/greg-tests/aws-cloudformation-templates/-/jobs/1151565819)         |
| [kube-linter](https://github.com/stackrox/kube-linter)                   | Apache 2.0 | Go       | Y    |       | [Job Template](https://gitlab.com/greg/sastiac/-/raw/master/kube-linter.yml)                                                                                            |
| [puppet-lint](https://github.com/rodjek/puppet-lint)                     | MIT        | Ruby     | Y    |       | [Job Template](https://gitlab.com/greg/sastiac/-/raw/master/puppet-lint.yml) + [Logs](https://gitlab.com/greg-tests/puppet-examples/-/jobs/1151570543)                  |
| [terrascan](https://github.com/accurics/terrascan)                       | Apache 2.0 | Go       | Y    |       | [Job Template](https://gitlab.com/greg/sastiac/-/raw/master/terrascan.yml) + [Logs](https://gitlab.com/greg-tests/tfsec-example-project/-/jobs/1151566295)              |
| [tflint](https://github.com/terraform-linters/tflint)                    | MPL 2.0    | Go       | Y    |       | [Job Template](https://gitlab.com/greg/sastiac/-/raw/master/tflint.yml)                                                                                                 |
| [tfsec](https://github.com/tfsec/tfsec)                                  | MIT        | Go       | Y    |       | [Job Template](https://gitlab.com/greg/sastiac/-/raw/master/tfsec.yml) + [Logs](https://gitlab.com/greg-tests/tfsec-example-project/-/jobs/1151566296)                  |

## Usage

These CI jobs are not officially supported and are not suggested for use outside of testing purposes.

To test them out, you can `include:remote` the template.

```yaml
include:
  - remote: https://gitlab.com/greg/sastiac/-/raw/master/ansible-lint.yml
  - remote: https://gitlab.com/greg/sastiac/-/raw/master/cfn-scan.yml
  - remote: https://gitlab.com/greg/sastiac/-/raw/master/cfn-python-lint.yml
  - remote: https://gitlab.com/greg/sastiac/-/raw/master/checkov.yml
  - remote: https://gitlab.com/greg/sastiac/-/raw/master/kube-linter.yml
  - remote: https://gitlab.com/greg/sastiac/-/raw/master/puppet-lint.yml
  - remote: https://gitlab.com/greg/sastiac/-/raw/master/terrascan.yml
  - remote: https://gitlab.com/greg/sastiac/-/raw/master/tflint.yml
  - remote: https://gitlab.com/greg/sastiac/-/raw/master/tfsec.yml
```

Contributions welcome!
